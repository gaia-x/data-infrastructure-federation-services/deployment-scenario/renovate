# Renovate for GXFS-FR projects

This project takes care of maintaining gxfs-fr projects dependencies up-to-date with Renovate bot.
It is configured (on gitlab.com) as a scheduled CI/CD job to regularly scan through all to be continuous projects
dependencies. It automatically detects new released versions. It proposes upgrading those with simple Merge Requests.

# How to configure renovate bot on your project

## Step 1 - Project on-boarding
By default, this bot will scan projects under deployment-scenario group. At first execution, It will onboard these projects. It will then create an On-boarding Merge Request

<figure>
  <img
  src="./assets/onboarding_merge_request.png"
  alt="Renovate bot - onboarding Merge Request.">
  <figcaption>Renovate bot - Onboarding Merge Request</figcaption>
</figure>


To accept renovate bot, you simply have to merge this merge request.
However, if you _do_ not want to enable renovate bot on your project, you have to close this merge request.

> **Note:** This Merge Request will create a new file named renovate.json at the root of your project. It helps to configure behavior of renovate bot on your project with a default configuration.
>
>```javascript
>{
>  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
>  "extends": [
>    "config:recommended"
>  ]
>}
>```

In this Merge Request, renovate bot will also give you the list of dependencies that have changed. When you accept this MR, renovate bot will create by default one Merge Request per dependency. You can override this behavior by adding a specific configuration on your project.

> **Note:** Renovate bot also creates an issue called `Dependency Dashboard`. This issue helps us to track dependencies MR. By default, there is a rate limiting on Gitlab API so this dashboard can help to see if some MR cannot be created.

<figure>
  <img
  src="./assets/dependency_dashboard.png"
  alt="Renovate bot - dependency dashboard.">
  <figcaption>Renovate bot - Dependency Dashboard</figcaption>
</figure>


# Step 2 - Project configuration
By default, Renovate bot will create one Merge Request per dependency to update. It can be tedious when your project has a lot of dependencies.

You can regroup in that case your dependencies in one MR by providing a packageRule specific to your language. In the following configuration, I specify that for my python project, I use poetry to manage dependencies and I will only accept minor or patch updates:

```javascript
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "config:recommended",
  ],
  "packageRules": [
    {
      "groupName": "python dependencies",
      "matchManagers": [
        "poetry"
      ],
      "matchUpdateTypes": [
        "minor",
        "patch"
      ],
    }
  ]
}

```

To configure properly your project dependending on your language, I invite you to check on renovate bot configuration:
- [Docker](https://docs.renovatebot.com/docker/)
- [Java](https://docs.renovatebot.com/java/)
- [JavaScript](https://docs.renovatebot.com/javascript/)
- [Python](https://docs.renovatebot.com/python/)
- [NodeJS](https://docs.renovatebot.com/node/)

You can also specify some options to add labels on your merge request, if your MR can be automatically applied when pipelines are successful or rebase MR in case of changes detected on base branch.

As an example, I configured project `provider-catalogue` with this renovate.json configuration:

```javascript
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "config:recommended",
    ":rebaseStalePrs",
    ":enableVulnerabilityAlertsWithLabel('security')"
  ],
  "labels": [
    "dependencies"
  ],
  "assigneesFromCodeOwners": true,
  "semanticCommits": "enabled",
  "packageRules": [
    {
      "matchPackagePatterns": [
        "*"
      ],
      "semanticCommitType": "chore"
    },
    {
      "groupName": "to-be-continuous dependencies",
      "groupSlug": "tbc-deps",
      "matchPackagePatterns": [
        "to-be-continuous/*"
      ],
      "matchUpdateTypes": [
        "minor",
        "patch"
      ],
      "semanticCommitType": "chore",
      "automerge": true,
      "automergeType": "pr",
      "platformAutomerge": true,
      "rebaseWhen": "auto"
     },
    {
      "groupName": "docker dependencies",
      "groupSlug": "docker-deps",
      "matchDatasources": ["docker"],
      "matchUpdateTypes": [
        "minor",
        "patch"
      ],
      "semanticCommitType": "chore",
      "automerge": true,
      "automergeType": "pr",
      "platformAutomerge": true,
      "rebaseWhen": "auto"
     },
    {
      "groupName": "python dependencies",
      "groupSlug": "python-deps",
      "matchManagers": [
        "poetry"
      ],
      "matchUpdateTypes": [
        "minor",
        "patch"
      ],
      "automerge": true,
      "automergeType": "pr",
      "platformAutomerge": true,
      "rebaseWhen": "auto"
    }
  ]
}

```

# References
- [Official documentation on Renovate Bot](https://docs.renovatebot.com)
- [An introduction article on how to manage Renovate Bot](https://blog.wescale.fr/comment-mettre-%C3%A0-jour-ses-d%C3%A9pendances-applicatives-avec-renovate)
- [Some notes about configurations](https://www.ederbit.xyz/notes/devops/RenovateBot)